bo_scripts = [{'name': 'Agent status', 'key': 'agent.ping', 'required level': 0},
              {'name': 'Host name', 'key': 'agent.hostname', 'required level': 0},
                {'name': 'ifconfig', 'key': 'system.run[ifconfig]', 'required level': 0},
                {'name': 'top -n1', 'key': 'system.run["top -n1"]', 'required level': 0},
                {'name': 'tcvnc', 'key': 'system.run[tcvnc]', 'required level': 0}]
