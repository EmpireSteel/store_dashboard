# coding=utf-8

from flask import render_template, redirect, request, url_for, json
from app import app
from app.store_class import Store
from app.utm import utm
from subprocess import check_output
from flask_login import login_required
from app.models import User
from app.scripts import bo_scripts


store = Store()
user = None


@app.route('/user/<username>')
@login_required
def user_page(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user.html', user=user, store=store)



@app.route('/test')
@login_required
def test():
        store.get_init('dashboard', 'bo-3001')
        print(str(store.store))
        return render_template('dashboard_new.html', store=store)


@app.route('/')
@login_required
def main():
    search = request.args.get('store')
    if not search:
        store.get_bo_hostgroups()
        store.get_s_hostgroups()
        store.get_g_hostgroups()
        store.get_x5_stores()
        return render_template('main_page.html', store=store)
    else:
        search = store.detect(search)
        return redirect(url_for('pages', page='dashboard', server=search))


@app.route('/utm/')
def utm_restart():
    data = utm()
    return render_template('utm.html', stores=data)


@app.route('/get_action', methods=['GET', 'POST'])
@login_required
def get_action():
    ip = request.form['name']
    key = request.form['key']
    ip = ip.split(':')[1]
    try:
        answer = check_output('/disk1/x5/zabbix/bin/zabbix_get -s ' + ip + ' -k ' + key, shell=True)
        answer = answer.decode("utf-8")
        answer.strip()
        if '\x1b[0' in answer:
            answer = answer.split('\x1b[0')
            for i in range(len(answer)):
                answer[i] = answer[i][5:]
    except Exception:
        answer = "Command Error"
    return json.dumps({'answer': answer})


@app.route('/<page>/<server>')
@login_required
def pages(page, server):
    search = request.args.get('store')
    if server.upper() != store.host.upper():
        server = store.detect(server)
        return redirect(url_for('pages', page=page, server=server))
    else:
        if not search:
            store.get_init(page, server)
            return render_template(page + '.html', store=store, scripts=bo_scripts)
        else:
            return redirect(url_for('pages', page=page, server=search.upper()))


@app.errorhandler(500)
def page_not_found(e):
    return render_template('500.html'), 500


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404







