# coding=utf-8
from pyzabbix import ZabbixAPI
import os
import datetime
import psycopg2
from app import chart_plot


bo_rule = ('3', '4', '5', '6', '7', '8', '9', '4', 'L', 'Q', 'G', 'J', 'S', 'X', 'Y', 'H', 'E', 'O', 'V', 'D')
bf_rule1 = ('1', '2', 'B')
bf_rule2 = ('4000', '4001', '4002', '4003', '4004', '4005', '4006')


class Store:
    def __init__(self):
        self.bo_zabbix = ZabbixAPI('http://msk-dpro-app351/')
        self.bo_zabbix.login("store_card_api", "12345")
        self.bf_zabbix = ZabbixAPI('http://zabbix-head.x5.ru/')
        self.bf_zabbix.login("store_card_api", "12345")
        self.host = ''
        self.type = ''
        self.bo_hostgroups = ''
        self.s_hostgroups = ''
        self.g_hostgroups = ''
        self.bo_applications = {}
        self.pos_applications = {}
        self.store = {}
        self.colors = ['#FF0000', '#FFFF00', '#008000', '#800000#0000FF', '#808080',
                       '#808000', '#00FF00', '#C0C0C0', '#008080', '#0000FF',
                       '#000080', '#FF00FF', '#800080', '#33E3FF', '#FF33A5',
                       '#000000']
        self.version = '2.3.3'

    def get_x5_stores(self):
        self.x5_stores = ''
        answer = self.bo_zabbix.do_request('template.get',
                          {'templateids': '130594','selectHosts': ['name'], 'output':['hosts']})['result'][0]['hosts']
        self.x5_stores = [i['name'] for i in answer]

    def get_bo_hostgroups(self):
            self.bo_hostgroups = self.bo_zabbix.do_request('hostgroup.get', {
                'search': {
                    'name': 'RU/Филиал'
                },
                'output': ['groupid', 'name'],
                'sortfield': 'name'
            })['result']
            for i in self.bo_hostgroups:
                i['name'] = i['name'][3:]
            for i in self.bo_hostgroups:
                i['hosts'] = []
                i['hosts'].extend(self.bo_zabbix.do_request('host.get', {
                    'groupids': i['groupid'],
                    'search': {'host': 'BO*'},
                    'searchWildcardsEnabled': True,
                    'output': ['host'],
                    'sortfield': 'host'
                })['result'])

    def get_s_hostgroups(self):
            self.s_hostgroups = self.bf_zabbix.do_request('hostgroup.get', {
                'search': {
                    'name': 'Филиал'

                },
                'output': ['groupid', 'name'],
                'sortfield': 'name'
            })['result']
            for i in self.s_hostgroups:
                i['hosts'] = []
                i['hosts'].extend(self.bf_zabbix.do_request('host.get', {
                    'groupids': [i['groupid'], '14'],
                    'monitored_hosts': 'true',
                    'search': {'host': 'Super*'},
                    'searchWildcardsEnabled': True,
                    'output': ['host'],
                    'sortfield': 'host'
                })['result'])

    def get_g_hostgroups(self):
            self.g_hostgroups = self.bf_zabbix.do_request('hostgroup.get', {
                'search': {
                    'name': 'Филиал'

                },
                'output': ['groupid', 'name'],
                'sortfield': 'name'
            })['result']
            for i in self.g_hostgroups:
                i['hosts'] = []
                i['hosts'].extend(self.bf_zabbix.do_request('host.get', {
                    'groupids': [i['groupid'], '12'],
                    'monitored_hosts': 'true',
                    'search': {'host': 'Giper*'},
                    'searchWildcardsEnabled': True,
                    'output': ['host'],
                    'sortfield': 'host'
                })['result'])

    def get_bf_full_name(self, name):
        find = self.bf_zabbix.do_request('host.get', {
            'search': {
                'name': name
            },
            'monitored_hosts': 'true',
            'output': ['name']
        })['result']
        return find[0]['name']

    def get_init(self, page, server):
        self.host = server.upper()
        print(page)
        print(server)
        if self.type == 'bo':
            self.get_bo_items()
            self.get_bo_applications()
            self.get_bo_pos_applications()
            if page == 'dashboard':
                self.get_bo_pos_ping_status()
                self.get_bo_uptime()
                self.get_bo_problems()
                self.store['server_graphics'] = chart_plot.get_bo_data(self.store, self.bo_zabbix)
            elif page == 'server':
                self.get_bo_bacchus()
                self.get_bo_processor()
                self.get_bo_utm_monitor()
                self.get_bo_system()
                self.get_bo_memory()
                self.get_bo_os()
                self.get_bo_product()
                self.get_bo_ram()
                self.get_close_day()

            elif page == 'poses':
                self.get_bo_pos_bacchus()
                self.get_bo_pos_processor()
                self.get_bo_pos_fz54()
                self.get_bo_pos_os()
                self.get_bo_pos_ram()
                self.get_bo_pos_memory()
                self.get_bo_pos_system()
                self.get_bo_pos_product()
                self.store['poses']['history'] = chart_plot.get_pos_data(self.store['poses'], self.bo_zabbix)
                self.get_last_check()
                self.get_last_alk()
                self.get_z()
                print()

            elif page == 'network':
                self.get_bo_network()
            elif page == 'promo':
                self.get_promo()
                self.get_null_prices()
        elif self.type == 'bf':
            self.get_bf_items()
            self.get_bf_applications()
            if page == 'dashboard':
                self.get_bf_scales()
                self.get_bf_problems()
                self.get_bf_applications()

    def get_bf_items(self):
        self.store = {'servers': [], 'poses': []}
        find = self.bf_zabbix.do_request('host.get', {
            'search': {
                'name': self.host
            },
            'selectInterfaces': ['ip', 'dns'],
            'output': 'extend'
        })['result']
        self.store['hostid'] = find[0]['hostid']
        items = [{'ip': i['ip'], 'name': i['dns']} for i in find[0]['interfaces']]
        items = sorted(items, key=lambda k: int(k['ip'].split('.')[-1]))
        for i in items:
            if i['name'].startswith('BO') or i['name'].startswith('ORA') or i['name'].startswith('APPSRV') or\
                    i['name'].startswith('CASH') or i['name'].startswith('UTM'):
                self.store['servers'].append(i)
            elif i['name'].startswith('POS'):
                self.store['poses'].append(i)
            elif i['name'].startswith('APC'):
                self.store['apc'] = i
        self.store['servers'] = self.get_ping_status(self.store['servers'])
        self.store['poses'] = self.get_ping_status(self.store['poses'])
        try:
            self.store['apc'] = self.get_ping_status(self.store['apc'])
        except:
            self.store['apc'] = []

    def get_bf_problems(self):
        self.store['problems'] = []
        find = self.bf_zabbix.do_request("trigger.get", {
            "hostids": self.store['hostid'],
            "expandDescription": "1",
            "output": 'extend',
            "filter": {'value': 1},
        })['result']
        for i in find:
            self.store['problems'].append({'name': i['description'],
                                           'date': datetime.datetime.fromtimestamp(int(i['lastchange']))
                                          .strftime('%Y-%m-%d %H:%M:%S')})
        self.store['problems'] = sorted(self.store['problems'], key=lambda k: k['date'], reverse=True)

    def get_bf_applications(self):
        self.store['bf_applications'] = {}
        applications = self.bf_zabbix.do_request('application.get', {
            "hostids": self.store['hostid'],
            'output': ['name']
        })['result']
        for i in applications:
            self.store['bf_applications'][i['name']] = i['applicationid']

    def get_bf_scales(self):
        self.store['scales'] = []
        try:
            find = self.bf_zabbix.do_request("application.get", {
                "applicationids": self.store['bf_applications']['Весы'],
                'selectItems': ['name', 'lastvalue', 'status']
            })['result'][0]['items']
            items = []
            for i in range(len(find)):
                if find[i]['status'] == '0':
                    items.append(find[i])
            ip = self.bf_zabbix.do_request("host.get", {
                'hostids': self.store['hostid'],
                'selectMacros': ['macro', 'value']
            })['result'][0]['macros']
            for i in find:
                for j in ip:
                    if i['name'].split(': ')[-1] == j['macro']:
                        item = {'name': 'Весы', 'ip': j['value']}
                        if i['lastvalue'] == '1':
                            item['status'] = 'online'
                        else:
                            item['status'] = 'offline'
                        self.store['scales'].append(item)
            self.store['scales'] = sorted(self.store['scales'], key=lambda k: int(k['ip'].split('.')[-1]))
        except:
            find = self.bf_zabbix.do_request("application.get", {
                "applicationids": self.store['bf_applications']['Сервер CASH'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            for i in find:
                if i['name'].startswith('Ping'):
                    item = {'name': 'Весы', 'ip': i['name'].split(': ')[-1]}
                    if i['lastvalue'] == '1':
                        item['status'] = 'online'
                    else:
                        item['status'] = 'offline'
                    self.store['scales'].append(item)
                    self.store['scales'] = sorted(self.store['scales'], key=lambda k: int(k['ip'].split('.')[-1]))






    # ------------------------------------------Общее-------------------------------------------------

    def detect(self, name):
        if name.upper().startswith("BO"):
            self.type = 'bo'
        elif name.upper().startswith("SUPER") or name.upper().startswith("GIPER"):
            self.type = 'bf'
            name = self.get_bf_full_name(name)
        else:
            if name.upper().startswith(bf_rule1) or name.upper() in bf_rule2:
                self.type = 'bf'
                name = self.get_bf_full_name(name)
            elif name.upper().startswith(bo_rule):
                name = 'BO-' + name
                self.type = 'bo'
        self.host = name
        return name


    def get_ping_status(self, item):
        try:
            for i in item:
                i['status'] = 'online' if os.system("fping -t 1 " + i['ip']) == 0 else 'offline'
            return item
        except:
            item['status'] = 'online' if os.system("fping -t 1 " + item['ip']) == 0 else 'offline'
            return item



    # ------------------------------------------BO-------------------------------------------------

    def get_bo_applications(self):
        self.store['bo_applications'] = {}
        applications = self.bo_zabbix.do_request('application.get', {
            "hostids": self.store['bo']['hostid'],
            'output': ['name']
        })['result']
        for i in applications:
            self.store['bo_applications'][i['name']] = i['applicationid']

    def get_bo_pos_applications(self):
        self.store['pos_applications'] = []
        for i in self.store['poses']['hostid']:
            apps = {}
            applications = self.bo_zabbix.do_request('application.get', {
                "hostids": i,
                'output': ['name']
            })['result']
            for i in applications:
                apps[i['name']] = i['applicationid']
            self.store['pos_applications'].append(apps)

    def get_bo_items(self):
        find = self.bo_zabbix.do_request('host.get', {
            'search': {
                'name': self.host[3:]
            },
            'selectInterfaces': ['ip'],
            'output': 'extend'
        })['result']
        elements = [{'name': i['name'], 'hostid': i['hostid'], 'ip': i['interfaces'][0]['ip']} for i in find]
        elements = sorted(elements, key=lambda k: k['ip'])
        self.store['poses'] = {'name': [], 'hostid': [], 'ip': [], 'metrics': {}}
        for i in elements:
            if i['name'].startswith("BO-"):
                self.store['bo'] = i
            elif i['name'].startswith("CC"):
                self.store['cash'] = i
            elif "SAP-" + self.host[3:] in i['name']:
                self.store['bio'] = i
            elif i['name'].startswith("POS"):
                self.store['poses']['name'].append(i['name'])
                self.store['poses']['hostid'].append(i['hostid'])
                self.store['poses']['ip'].append(i['ip'])
        tsd = self.bo_zabbix.do_request('item.get', {'hostids': self.store['bo']['hostid'], 'search': {'key_': 'tsd.ip'}, 'output': ['name', 'lastvalue']})['result']
        self.store['tsd'] = [{'name': i['name'][7:], 'ip': i['lastvalue']} for i in tsd if str(i['lastvalue']) != '0']
        self.store['tsd'] = sorted(self.store['tsd'], key=lambda k: k['ip'])
        self.store['bo'] = self.get_ping_status(self.store['bo'])
        try:
            self.store['cash'] = self.get_ping_status(self.store['cash'])
        except:
            self.store['cash'] = []
        try:
            self.store['bio'] = self.get_ping_status(self.store['bio'])
        except:
            self.store['bio'] = []
        self.store['tsd'] = self.get_ping_status(self.store['tsd'])


    def get_bo_pos_ping_status(self):
        self.store['poses']['status'] = []
        for i in self.store['poses']['ip']:
            self.store['poses']['status'].append('online' if os.system("fping -t 1 " + i) == 0 else 'offline')

    def get_bo_uptime(self):
        self.store['bo']['uptime'] = self.bo_zabbix.do_request('item.get', {
            'hostids': self.store['bo']['hostid'],
            'search': {
                'key_': 'system.uptime'
            },
            'output': ['name', 'lastvalue']})["result"][0]['lastvalue']
        self.store['bo']['uptime'] = datetime.timedelta(seconds=int(self.store['bo']['uptime']))
        find = []
        for i in self.store['poses']['hostid']:
            find.extend(self.bo_zabbix.do_request('item.get', {
                'hostids': i,
                'search': {
                    'key_': 'system.uptime'
                },
                'output': ['name', 'lastvalue']})["result"])
        self.store['poses']['uptime'] = [datetime.timedelta(seconds=int(i['lastvalue'])) for i in find]

    def get_bo_bacchus(self):
        try:
            self.store['bo']['bacchus'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['Bacchus'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['bacchus'] = []

    def get_bo_processor(self):
        try:
            self.store['bo']['processor'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['Processor Information'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            for i in range(len(self.store['bo']['processor'])):
                if self.store['bo']['processor'][i]['name'] == 'Утилизация CPU':
                    self.store['bo']['processor'][i]['lastvalue'] = str(round(float(self.store['bo']['processor'][i]['lastvalue']), 2)) + " %"
        except:
            self.store['bo']['processor'] = []

    def get_bo_utm_monitor(self):
        try:
            self.store['bo']['utm_monitor'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['UTM-MONITOR'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['utm_monitor'] = []

    def get_bo_system(self):
        try:
            self.store['bo']['system'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['System'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            for i in range(len(self.store['bo']['system'])):
                if self.store['bo']['system'][i]['name'] == 'Аптайм':
                    self.store['bo']['system'][i]['lastvalue'] = datetime.timedelta(seconds=int(self.store['bo']['system'][i]['lastvalue']))
                if self.store['bo']['system'][i]['name'] == 'Местное время':
                    self.store['bo']['system'][i]['lastvalue'] = datetime.datetime.fromtimestamp(int(self.store['bo']['system'][i]['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S')
        except:
            self.store['bo']['system'] = []

    def get_bo_memory(self):
        try:
            self.store['bo']['memory'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['Memory'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            for i in self.store['bo']['memory']:
                if i['name'] == "Общее количество дискового пространства в /" or \
                        i['name'] == "Свободно дискового пространства в / (GB)":
                    i['lastvalue'] = str(round(float(i['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb"
                if i['name'] == 'Свободно дискового пространства в / (в процентах)':
                    i['lastvalue'] = str(round(float(i['lastvalue']), 2)) + " %"
        except:
            self.store['bo']['memory'] = []

    def get_bo_os(self):
        try:
            self.store['bo']['os'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['OS Information'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['os'] = []

    def get_bo_product(self):
        try:
            self.store['bo']['product'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['Product Information'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['product'] = []

    def get_bo_ram(self):
        try:
            self.store['bo']['ram'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['RAM'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            for i in self.store['bo']['ram']:
                if i['name'] == 'Количество используемой памяти' \
                        or i['name'] == 'Общее количество памяти' or i['name'] == 'Свободно памяти':
                    i['lastvalue'] = str(round(float(i['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb"
                if i['name'] == 'Количество свободной памяти файла подкачки (в процентах)':
                    i['lastvalue'] = str(round(float(i['lastvalue']), 2)) + " %"
        except:
            self.store['bo']['ram'] = []

    def get_bo_network(self):
        try:
            self.store['bo']['network'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.store['bo_applications']['Router'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['network'] = []

    def get_close_day(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT ra."create_timestamp"
        FROM gk_report_archive ra
        WHERE ra."type_code" = 'XRG_TILL_FINANCIAL'
        ORDER BY ra."create_timestamp" DESC
        LIMIT 1""")
        records = cursor.fetchall()
        conn.close()
        self.store['bo']['close_day'] = [{'date': i[0]} for i in records]

    def get_bo_pos_bacchus(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Bacchus'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                print(result)
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if j['name'] == 'Касса использует Bacchus(gk_functions.xml)':
                        if j['lastvalue'] == 'GK':
                            items[j['name']].append('true')
                        elif j['lastvalue'] == 'SAP_PI':
                            items[j['name']].append('false')
                        else:
                            items[j['name']].append('не поддерживается')
                    else:
                        if j['lastvalue'] == '0':
                            items[j['name']].append('не поддерживается')
                        else:
                            items[j['name']].append(j['lastvalue'])
            print(items)
            self.store['poses']['metrics']['bacchus'] = items
        except:
            self.store['poses']['metrics']['bacchus'] = {
                '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}
        print(self.store['poses']['metrics']['bacchus'])

    def get_bo_pos_processor(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Processor Information'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']


                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []

                    if j['name'] == 'Утилизация CPU':
                        items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                    else:
                        if len(j['lastvalue']) >= 18:
                            items[j['name']].append(j['lastvalue'][:18] + "\n" + j['lastvalue'][18:])
                        else:
                            items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Процессор'] = items
        except:
            self.store['poses']['metrics']['Процессор'] = {'': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}


    def get_bo_pos_fz54(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['FZ54'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if j['name'] == 'Дата активизации ФН':
                        items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                    elif j['name'] == 'Дата последнего документа отправленного в ОФД':
                        if int(j['lastvalue']) != 0:
                            items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                        else:
                            items[j['name']].append('никогда')
                    elif j['name'] == 'Дата создания файла info_FR_human':
                        if int(j['lastvalue']) != 0:
                            items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                        else:
                            items[j['name']].append('никогда')
                    else:
                        items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['ФЗ54'] = items
        except :
            self.store['poses']['metrics']['ФЗ54'] = {'': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_init(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Initialization'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Инициализация устройств'] = items
        except:
            self.store['poses']['metrics']['Инициализация устройств'] = {'': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_os(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['OS Information'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Операционная система'] = items
        except:
            self.store['poses']['metrics']['Операционная система'] = {'': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_ram(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['RAM'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if "в процентах" in j['name']:
                        items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                    else:
                        items[j['name']].append(str(round(float(j['lastvalue']) / 1024 / 1024, 2)) + " Mb")
            self.store['poses']['metrics']['Оперативная память'] = items
        except:
            self.store['poses']['metrics']['Оперативная память'] = {
            '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_memory(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Memory'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if "в процентах" in j['name']:
                        items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                    elif "RAID" in j['name']:
                        items[j['name']].append(j['lastvalue'])
                    else:
                        items[j['name']].append(str(round(float(j['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb")
            self.store['poses']['metrics']['Память'] = items
        except:
            self.store['poses']['metrics']['Память'] = {
            '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_system(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['System'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if j['name'] == 'Аптайм':
                        items[j['name']].append(datetime.timedelta(seconds=int(j['lastvalue'])))
                    elif j['name'] == 'Местное время':
                        items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                    else:
                        items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Система'] = items
        except:
            self.store['poses']['metrics']['Система'] = {
                '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_product(self):
        try:
            items = {}
            for i in self.store['pos_applications']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Product Information'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Устройство'] = items
        except:
            self.store['poses']['metrics']['Устройство'] = {
                '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_problems(self):
        self.store['problems'] = []
        bo_find = self.bo_zabbix.do_request("trigger.get", {
            "hostids": self.store['bo']['hostid'],
            "expandDescription": "1",
            "output": 'extend',
            "filter": {'value': 1},
        })['result']
        for i in bo_find:
            self.store['problems'].append({'name': i['description'], 'host': self.store['bo']['name'],
                                   'date': datetime.datetime.fromtimestamp(int(i['lastchange']))
                                    .strftime('%Y-%m-%d %H:%M:%S')})
        for i in self.store['poses']['name']:
            pos_find = self.bo_zabbix.do_request("trigger.get", {
                "host": i,
                "expandDescription": "1",
                "output": 'extend',
                "filter": {'value': 1},
            })['result']
            for j in pos_find:
                self.store['problems'].append({'name': j['description'], 'host': i,
                                      'date': datetime.datetime.fromtimestamp(int(j['lastchange']))
                                        .strftime('%Y-%m-%d %H:%M:%S')})
        self.store['problems'] = sorted(self.store['problems'], key=lambda k: k['date'], reverse=True)

    def get_last_check(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT max(bk.aktdat), bk."workstation_id"
        FROM "gkretail"."gk_bonkopf" bk
        GROUP BY bk."workstation_id"
        ORDER BY bk."workstation_id"
""")
        records = cursor.fetchall()
        records = records[1:]
        conn.close()
        self.store['poses']['last_check'] = [{'pos': i, 'date': 'нет данных'} for i in self.store['poses']['name']]
        for i in self.store['poses']['last_check']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]

    def get_last_alk(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT DISTINCT on (y.workstation_id) max(y."aktdat"), y.workstation_id, nn.nm_itm
        FROM gkretail.gk_bonkopf y, gkretail.gk_bonposition z, gkretail.as_itm nn
        WHERE
        z.artnr IN (SELECT item_id FROM xrg_item WHERE alcohol_flag = 'J' and TAX_INPUT_REQUIRED_FLAG='J')
        AND z.bon_seq_id = y.bon_seq_id
        AND nn.id_itm = z.artnr
        GROUP BY y.workstation_id, y.aktdat, nn.nm_itm
        ORDER BY y.workstation_id ASC, y.aktdat DESC""")
        records = cursor.fetchall()
        conn.close()
        self.store['poses']['last_alk'] = [{'pos': i, 'date': 'нет данных', 'item': 'нет данных'} for i in self.store['poses']['name']]
        for i in self.store['poses']['last_alk']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]
                    i['item'] = j[2]

    def get_z(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT DISTINCT on (b."lade_nr") max(b."aktdat"), b."lade_nr"
        FROM "gkretail"."gk_bonkopf" b
        WHERE b."workstation_id" = 0
        AND b."belegtyp" = 501
        GROUP BY b."lade_nr", "aktdat"
        ORDER BY b."lade_nr" ASC, b."aktdat" DESC""")
        records = cursor.fetchall()
        conn.close()
        self.store['poses']['z'] = [{'pos': i, 'date': 'нет данных'} for i in self.store['poses']['name']]
        for i in self.store['poses']['z']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]

    def get_null_prices(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        select ids.item_id, names.de_itm, prices.price_amount
        from xrg_item as ids 
        join as_itm as names on ids.item_id=names.id_itm
        join gk_item_selling_prices as prices on ids.item_id=prices.item_id
        where 
        prices.price_amount = 0
        order by ids.item_id""")
        records = cursor.fetchall()
        conn.close()
        self.store['null_prices'] = []
        for i in records:
            self.store['null_prices'].append({'ids': i[0], 'item': i[1]})

    def get_promo(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
            SELECT "nm_prm_prt", "description"
            FROM "gkretail"."co_prm"
            ORDER BY "nm_prm_prt" DESC """)
        records = cursor.fetchall()
        conn.close()
        self.store['promo'] = []
        for i in records:
            self.store['promo'].append({'name': i[0], 'description': i[1]})