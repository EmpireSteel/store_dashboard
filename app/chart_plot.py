import datetime
import time


def get_bo_data(store, zabbix):
    find = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    find.extend(zabbix.do_request('item.get',{
        'hostids': [store['bo']['hostid']],
        'search':{'name': 'Утилизация CPU'},
        'output': ['name']
    })['result'])
    find.extend(zabbix.do_request('item.get',{
        'hostids': [store['bo']['hostid']],
        'search':{'name': 'Свободно памяти (в процентах)'},
        'output': ['name']
    })['result'])
    find.extend(zabbix.do_request('item.get',{
        'hostids': [store['bo']['hostid']],
        'search':{'name': 'Свободно дискового пространства в / (в процентах)'},
        'output': ['name']
    })['result'])

    find.extend(zabbix.do_request('item.get',{
        'hostids': [store['bo']['hostid']],
        'search':{'name': 'Количество свободной памяти файла подкачки (в процентах)'},
        'output': ['name']
    })['result'])
    for i in range(len(find)):
        find[i]['data'] = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": find[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']

    for i in range(len(find)):
        find[i]['time'] = [str(datetime.datetime.fromtimestamp(int(j['clock'])).strftime('%m-%d %H:%M')) for j in find[i]['data']]
        find[i]['values'] = [int(float(j['value'])) for j in find[i]['data']]
        find[i]['data'].clear()
    return find


def get_pos_data(pos, zabbix):
    pos['history'] = {}
    metrics = ['Утилизация CPU', 'Свободно памяти (в процентах)', 'Количество свободной памяти файла подкачки (в процентах)',
            'Свободно дискового пространства в / (в процентах)']
    for i in metrics:
        pos['history'][i] = [[] for i in pos['hostid']]
        temp = []
        time_from = time.time() - (1 * 6 * 60 * 60)
        temp.extend(zabbix.do_request('item.get',{
            'hostids': pos['hostid'],
            'search':{'name': i},
            'output': ['itemid']
        })['result'])
        for j in range(len(temp)):
            find = zabbix.do_request('history.get', {
                "history": 0,
                "itemids": temp[j]['itemid'],
                'time_from': int(time_from),
                "sortfield": "clock"
            })['result']
            for k in find:
                pos['history'][i][j].append({'x': int(k['clock']), 'y': k['value']})
    print(pos['history'])
    return pos['history']








