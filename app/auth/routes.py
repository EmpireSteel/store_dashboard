from flask import render_template, redirect, url_for, flash, request
from werkzeug.urls import url_parse
from flask_login import login_user, logout_user, current_user
from app import db
from app.auth import bp
from app.auth.forms import LoginForm, RegistrationForm, CheckForm
from app.ad import checkUserInAD
from app.models import User


@bp.route('/check', methods=['GET', 'POST'])
def check():
    if current_user.is_authenticated:
        return redirect(url_for('main'))
    global check_email
    check_email = {'email': '', 'status': False}
    form = CheckForm()
    if form.validate_on_submit():
        if checkUserInAD(form.email.data, form.password.data):
            check = User.query.filter_by(email=form.email.data).first()
            if check is not None:
                flash('Пользователь с таким email уже зарегистирован.')
                return redirect(url_for('auth.check'))
            check_email = {'email': form.email.data, 'status': True}
            flash('Заполните поля для регистрации')
            return redirect(url_for('auth.registration', email=form.email.data))
        else:
            flash('Неверный логин или пароль. Введите данные от корпоративного профиля.')
            return redirect(url_for('auth.check'))
    return render_template('user_check.html', form=form)


@bp.route('/registration/<email>', methods=['GET', 'POST'])
def registration(email):
    if current_user.is_authenticated:
        return redirect(url_for('main_page'))
    if check_email['email'] != email or check_email['status'] == False:
        return redirect(url_for('auth.check'))
    form = RegistrationForm()
    form.username.data=email.split('@')[0]
    if form.validate_on_submit():
        check = User.query.filter_by(username=form.username.data).first()
        if check is not None:
            flash('Пользователь с таким именем уже зарегистирован.')
            return redirect(url_for('registration', email=email))
        user = User(username=email.split('@')[0], email=email, tel=form.tel.data, level=0)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Вы зарегистрированы')
        return redirect(url_for('auth.login'))
    return render_template('user_check.html', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('auth.login'))


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if form.username.data == 'admin' and form.password.data == 'admin':
            login_user(user)
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('main')
            return redirect(next_page)
        else:
            try:
                if user is None or not user.check_password(form.password.data):
                    flash('Неверное имя или пароль')
                    return redirect(url_for('auth.login'))
            except:
                flash('Неверное имя или пароль')
                return redirect(url_for('auth.login'))
            login_user(user, remember=form.remember_me.data)
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('main')
            return redirect(next_page)
    return render_template('user_login.html', form=form)
