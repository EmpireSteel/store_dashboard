from pyzabbix import ZabbixAPI


def utm():
    z = ZabbixAPI('http://msk-dpro-app351')
    z.login("andrey.gavrilov", "200994")
    month = z.do_request('item.get', {
        'filter': {'name': 'Кол-во перезагрузок за месяц'},
        'searchWildcardsEnabled': 'true',
        'selectHosts': ['name'],
        'output': ['lastvalue', 'hosts']
    })['result']
    stores = [{'hostid': i['hosts'][0]['hostid'],'name': i['hosts'][0]['name'], 'month': int(i['lastvalue'])} for i in month]
    stores = sorted(stores, key=lambda k: k['month'], reverse=True)
    stores = stores[:26]
    for i in range(len(stores)):
        find = z.do_request('application.get', {
            'hostids': stores[i]['hostid'],
            'filter': {'name': 'UTM-RESTART-MONITOR'},
            'selectItems': ['name', 'lastvalue'],
            'output': ['items']
        })['result'][0]['items']
        for j in find:
            if j['name'] == 'Кол-во перезагрузок за месяц':
                stores[i]['month'] = int(j['lastvalue'])
            elif j['name'] == 'Кол-во перезагрузок за предыдущий день':
                stores[i]['day'] = int(j['lastvalue'])
            elif j['name'] == 'Кол-во перезагрузок подряд':
                stores[i]['row'] = int(j['lastvalue'])
            elif j['name'] == 'Дата последней проверки':
                stores[i]['date'] = j['lastvalue']
    stores = sorted(stores, key=lambda k: k['month'], reverse=True)
    return stores
